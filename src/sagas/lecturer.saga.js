import {call, put, takeLatest} from 'redux-saga/effects';
import { listUsers } from '../services/admin/LecturerService';
import * as actions from "../redux/actions/lecturer.actions";

function* fetchLecturers({payload}) {
  try {
    const results = yield call(listUsers, payload);
    const { code, data, pageInfo } = results?.data;

    yield put({
      type: actions.FETCH_LECTURER_SUCCEEDED,
      payload: {
        code,
        data,
        pageInfo
      }
    });
  } catch (error) {
    yield put({
      type: actions.FETCH_LECTURER_FAILED,
      payload: error
    });
  } finally {
    yield put({
      type: actions.FETCH_LECTURER_REFRESH
    });
  }
}

export default function* lecturerSaga() {
  yield takeLatest(actions.FETCH_LECTURER_REQUEST, fetchLecturers);
}
