import { all, fork } from 'redux-saga/effects';

import authSaga from './auth.saga';
import lecturerSaga from './lecturer.saga';
import subjectSaga from './subject.saga';
import groupSaga from "./group.saga";

export default function* rootSaga() {
    yield all([
      authSaga(),
      lecturerSaga(),
      subjectSaga(),
      fork(groupSaga)
    ]);
}
