import {call, put, takeLatest} from 'redux-saga/effects';
import * as actions from "../redux/actions/subject.actions";
import {listSubjects} from "../services/admin/SubjectService";

function* fetchSubjects({payload}) {
  try {
    const results = yield call(listSubjects, payload);
    const { code, data, pageInfo } = results?.data;

    yield put({
      type: actions.FETCH_SUBJECT_SUCCEEDED,
      payload: {
        code,
        data,
        pageInfo
      }
    });
  } catch (error) {
    yield put({
      type: actions.FETCH_SUBJECT_FAILED,
      payload: error
    });
  } finally {
    yield put({
      type: actions.FETCH_SUBJECT_REFRESH
    });
  }
}

export default function* subjectSaga() {
  yield takeLatest(actions.FETCH_SUBJECT_REQUEST, fetchSubjects);
}
