import {call, put, takeLatest} from 'redux-saga/effects';
import * as actions from "../redux/actions/group.actions";
import {listGroups} from "../services/admin/GroupService";

function* fetchGroups({payload}) {
  try {
    const results = yield call(listGroups, payload);
    const { code, data, pageInfo } = results?.data;

    yield put({
      type: actions.FETCH_GROUP_SUCCEEDED,
      payload: {
        code,
        data,
        pageInfo
      }
    });
  } catch (error) {
    yield put({
      type: actions.FETCH_GROUP_FAILED,
      payload: error
    });
  } finally {
    yield put({
      type: actions.FETCH_GROUP_REFRESH
    });
  }
}

export default function* groupSaga() {
  yield takeLatest(actions.FETCH_GROUP_REQUEST, fetchGroups);
}
