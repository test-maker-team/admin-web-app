import GeneralService from "../GeneralService";
import { RootAPI } from "../../config/api.config";

export const listSubjects = ({page, size, query}) => {
  let params = {};
  if(page) params = {...params, page: page || 0};
  if(size) params = {...params, size: size || 25 };
  if(query) params = {...params, query: query.trim()};

  return GeneralService.get(`${RootAPI.SUBJECTS}/list`, {params: params});
}

export const createSubject = (data) => {
  return GeneralService.post(`${RootAPI.SUBJECTS}/create`, data);
}
