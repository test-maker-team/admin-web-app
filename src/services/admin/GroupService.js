import GeneralService from "../GeneralService";
import { RootAPI } from "../../config/api.config";

export const listGroups = ({page, size, query, lecturer}) => {
  let params = {};
  if(page) params = {...params, page: page || 0};
  if(size) params = {...params, size: size || 25 };
  if(query) params = {...params, query: query.trim()};

  return GeneralService.get(`${RootAPI.GROUPS}/list/${lecturer}`, {params: params});
}

export const createGroup = (data) => {
  return GeneralService.post(`${RootAPI.GROUPS}/create`, data);
}

export const deleteGroup = (username) => {
  return GeneralService.delete(`${RootAPI.GROUPS}/delete/${username}`);
}
