import GeneralService from "../GeneralService";
import { RootAPI } from "../../config/api.config";

export const listUsers = ({page, size, query}) => {
  let params = {};
  if(page) params = {...params, page: page || 0};
  if(size) params = {...params, size: size || 25 };
  if(query) params = {...params, query: query.trim()};

  return GeneralService.get(`${RootAPI.USER}/list`, {params: params});
}

export const createUser = (data) => {
  return GeneralService.post(`${RootAPI.USER}/create`, data);
}

export const deleteUser = (username) => {
  return GeneralService.delete(`${RootAPI.USER}/delete/${username}`);
}
