import React from "react";
import {CDropdownItem} from "@coreui/react";
import PropTypes from "prop-types";

export default function DropdownItem({title, icon, color, onClick}) {
  return (
    <CDropdownItem className="px-3 rounded" onClick={onClick}>
      {icon}
      <span className="mx-2" style={{color: color}}>{title}</span>
    </CDropdownItem>
  )
}

DropdownItem.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.any,
  color: PropTypes.string,
  onClick: PropTypes.func
}
