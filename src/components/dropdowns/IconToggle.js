import {DotsVerticalIcon} from "@heroicons/react/solid";
import {CDropdownToggle} from "@coreui/react";
import React from "react";

export default function IconToggle() {
  return (
    <CDropdownToggle className="p-0 m-0" caret={false}>
      <DotsVerticalIcon width={21} height={21}/>
    </CDropdownToggle>
  );
}
