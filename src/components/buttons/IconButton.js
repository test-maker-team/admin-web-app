import {Link} from "react-router-dom";
import {CTooltip} from "@coreui/react";
import React from "react";
import PropTypes from "prop-types";

const IconButton = ({tooltip, placement, children, onClick}) => {
  return (
    <CTooltip content={tooltip} placement={placement}>
      <Link className={'p-0 d-flex align-items-center'}
            to={'#'}
            onClick={onClick}>
        {children}
      </Link>
    </CTooltip>
  )
}

IconButton.propTypes = {
  tooltip: PropTypes.string,
  placement: PropTypes.string,
  onClick: PropTypes.func
}

export default IconButton;
