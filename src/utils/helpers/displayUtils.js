export const getBadge = status => {
  switch (status) {
    case 1: return 'success';
    case 0: return 'danger';
    default: return 'success';
  }
}
