import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from "@coreui/react";
import React from "react";

const GroupForm = () => {
  return (
    <CRow>
      <CCol xs={5}>
        <CCard>
          <CCardHeader className={'flex align-items-center'}>
            <span className="h4">Create Group</span>
          </CCardHeader>
          <CCardBody>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={7}>
        <CCard>
          <CCardHeader className={'flex align-items-center'}>
            <span className="h4">Add Student</span>
          </CCardHeader>
          <CCardBody>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default GroupForm;
