import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable, CDropdown, CDropdownMenu,
  CPagination,
  CRow,
} from "@coreui/react";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchRequest} from "../../redux/actions/subject.actions";

import {PencilAltIcon, TrashIcon} from '@heroicons/react/outline';
import SweetAlert from "sweetalert2";
import {getBadge} from "src/utils/helpers/displayUtils";
import IconToggle from "../../components/dropdowns/IconToggle";
import DropdownItem from "../../components/dropdowns/DropdownItem";

const SubjectsList = () => {
  const dispatch = useDispatch();
  const data = useSelector(state => state?.subject?.data);
  const pageInfo = useSelector(state => state?.subject?.pageInfo);

  useEffect(() => {
    dispatch(fetchRequest({page: 0, size: 25}));
  }, [dispatch])

  const listData = (page = 1, size = 25, query = '') => {
    page = (page === 0) ? 1 : page;
    dispatch(fetchRequest({page: page - 1, size, query}));
  }

  const handleDelete = (username) => {
    SweetAlert.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        SweetAlert.fire(
          'Deleted!',
          `Username ${username} has been deleted.`,
          'success'
        ).then(() => null)
      }
    })
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard>
          <CCardHeader className={'flex align-items-center'}>
            <span className="h4">List Subject</span>
            <div className={'card-header-actions'}>
              <CButton color="info">Add</CButton>
            </div>
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={data}
              fields={['id', 'courseName', 'status', 'actions']}
              hover
              striped
              itemsPerPage={pageInfo.itemsPerPage}
              activePage={pageInfo.currentPage}
              scopedSlots={{
                'status':
                  (item) => (
                    <td>
                      <CBadge color={getBadge(item.status)}>
                        {item.status === 1 ? 'Active' : 'Inactive'}
                      </CBadge>
                    </td>
                  ),
                'actions':
                  (item) => (
                    <td className={'d-flex align-items-center'}>
                      <CDropdown className="m-1">
                        <IconToggle/>
                        <CDropdownMenu className="p-1">
                          <DropdownItem title={'Edit'}
                                        icon={<PencilAltIcon width={21} height={21}/>}/>
                          <DropdownItem title={'Delete'}
                                        icon={<TrashIcon color={'#ff0000'} width={21} height={21}/>}
                                        color={'#ff0000'}
                                        onClick={() => handleDelete(item.username)}/>
                        </CDropdownMenu>
                      </CDropdown>
                    </td>
                  )
              }}
            />
            <CPagination
              activePage={pageInfo.currentPage + 1}
              onActivePageChange={(e) => listData(e, pageInfo.itemsPerPage)}
              pages={pageInfo.totalPages}
              doubleArrows={false}
              align="center"
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default SubjectsList;
