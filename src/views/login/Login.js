import { useEffect, useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CFormGroup,
  CInput,
  CInvalidFeedback,
  CLabel,
  CRow,
} from "@coreui/react";
import { useForm, Controller } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { loginRequest } from "../../redux/actions/auth.actions";
import { Redirect } from "react-router-dom";
import { RootPath } from "src/router/routes";

const Login = () => {
  const [redirectToReferrer, setRedirectToReferrer] = useState(false);
  const { control, handleSubmit, formState: { errors } } = useForm();
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state?.auth?.token);

  useEffect(() => {
    if (isLoggedIn) {
      setRedirectToReferrer(true);
    } else {
      setRedirectToReferrer(false);
    }
  }, [isLoggedIn]);

  const onSubmit = (values) => {
    dispatch(loginRequest(values));
  };

  return redirectToReferrer ? (
    <Redirect to={RootPath.DASHBOARD} />
  ) : (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol xs="11" sm="9" md="7" lg="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CFormGroup className="mb-2">
                      <CLabel htmlFor="username">Username</CLabel>
                      <Controller
                        name="username"
                        control={control}
                        defaultValue="kinhnguyen"
                        placeholder="Username"
                        rules={{ required: true }}
                        render={({ field }) => (
                          <CInput
                            {...field}
                            type="text"
                            invalid={errors.username?.type === 'required'}
                          />
                        )}
                      />
                      <CInvalidFeedback>{errors.username?.type === 'required' && "Username is required"}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup className="mb-2">
                      <CLabel htmlFor="password">Password</CLabel>
                      <Controller
                        name="password"
                        control={control}
                        defaultValue="zaq1@WSX"
                        placeholder="Password"
                        rules={{ required: true }}
                        render={({ field }) => (
                          <CInput
                            {...field}
                            type="password"
                            invalid={errors.password?.type === 'required'}
                          />
                        )}
                      />
                      <CInvalidFeedback>{errors.password?.type === 'required' && "Password is required"}</CInvalidFeedback>
                    </CFormGroup>
                    <CRow className="mt-4">
                      <CCol xs="6">
                        <CButton type="submit" color="primary" className="px-4">
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </form>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
