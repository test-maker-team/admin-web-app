import React, { useState } from 'react';
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { useDispatch } from 'react-redux';
import { logoutRequest } from 'src/redux/actions/auth.actions';
import { useEffect } from 'react';
import { capitalize, replace } from 'lodash';
import { useCookies } from 'react-cookie';

const TheHeaderDropdown = () => {
  const dispatch = useDispatch();
  const [cookies] = useCookies(['user_info']);
  const [user, setUser] = useState({ username: '', role: '' });

  useEffect(() => {
    const userInfo = cookies['user_info'];
    if (userInfo) {
      const { username, roles = [] } = userInfo;
      const roleName = capitalize(replace(roles[0].name, 'ROLE_', ''));
      if (username && roles) {
        setUser({ username, role: roleName });
      }
    }
  }, [cookies]);

  const handleLogout = () => dispatch(logoutRequest());

  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div>
          <span className="text-nowrap mr-2">{user.username}</span>
        </div>
        <div className="c-avatar">
          <CImg
            src={'avatars/dat-tran.jpg'}
            className="c-avatar-img"
            alt={user.username}
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-user" className="mfe-2" />Profile
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-settings" className="mfe-2" />
          Settings
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={handleLogout}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Logout
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
