import {Router} from "react-router-dom";
import {createBrowserHistory} from 'history';
import './scss/style.scss';
import Boot from "./redux/boot";
import AppRoute from "./router/AppRoute";

const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <AppRoute />
    </Router>
  );
}

Boot().then(() => App()).catch((error) => console.error(error));

export default App;
