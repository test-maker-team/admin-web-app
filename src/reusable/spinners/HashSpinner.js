import {HashLoader} from "react-spinners";

const HashSpinner = ({isLoading = true, size = 35}) => {

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      justifyItems: 'center'
    }}>
      <HashLoader
        color="#E97313"
        loading={isLoading}
        size={size}
      />
    </div>
  );
}

export default HashSpinner;
