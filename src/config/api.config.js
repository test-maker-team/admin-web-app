/*
*   DEFAULTED ENVIRONMENT IS PRODUCTION
*/
let baseURL = 'https://testmaker.top/api';

/*
*   SETTING ENVIRONMENT IS DEVELOPMENT
*/
if (process.env.NODE_ENV === 'development') {
    baseURL = 'http://localhost:8080/api/v1';
}

export default baseURL;

export const RootAPI = {
    ADMIN: `${baseURL}/admin`,
    STUDENT: `${baseURL}/student`,
    USER: `${baseURL}/users`,
    GROUPS: `${baseURL}/groups`,
    SUBJECTS: `${baseURL}/subjects`
}
