import {
  lazy,
  Suspense,
  useEffect,
  useState,
} from "react";
import {withRouter, Switch, BrowserRouter} from "react-router-dom";

import PrivateRoute from "../router/private.router";
import PublicRoute from "../router/public.router";
import {useDispatch, useSelector} from "react-redux";
import {checkAuthorization} from "../redux/actions/auth.actions";
import {RootPath} from "./routes";
import HashSpinner from "../reusable/spinners/HashSpinner";

const Login = lazy(() => import('../views/login/Login'));
const TheLayout = lazy(() => import('../containers/TheLayout'));

const AppRoute = () => {
  const dispatch = useDispatch();
  const token = useSelector(state => state?.auth.token)
  const [isLoggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    dispatch(checkAuthorization());
    if (token) {
      setLoggedIn(true);
    }
  }, [dispatch, isLoggedIn, token])

  return (
    <BrowserRouter>
      <Suspense fallback={<HashSpinner />}>
        <Switch>
          <PublicRoute path={RootPath.PAGE_LOGIN} component={Login}/>
          <PrivateRoute path={`/`} component={TheLayout}/>
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default withRouter(AppRoute);
