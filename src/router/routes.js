import loadable from '@loadable/component';

const Toaster = loadable(() => import('../views/notifications/toaster/Toaster'));
const Tables = loadable(() => import('../views/base/tables/Tables'));

const Breadcrumbs = loadable(() => import('../views/base/breadcrumbs/Breadcrumbs'));
const Cards = loadable(() => import('../views/base/cards/Cards'));
const Carousels = loadable(() => import('../views/base/carousels/Carousels'));
const Collapses = loadable(() => import('../views/base/collapses/Collapses'));
const BasicForms = loadable(() => import('../views/base/forms/BasicForms'));

const Jumbotrons = loadable(() => import('../views/base/jumbotrons/Jumbotrons'));
const ListGroups = loadable(() => import('../views/base/list-groups/ListGroups'));
const Navbars = loadable(() => import('../views/base/navbars/Navbars'));
const Navs = loadable(() => import('../views/base/navs/Navs'));
const Paginations = loadable(() => import('../views/base/paginations/Pagnations'));
const Popovers = loadable(() => import('../views/base/popovers/Popovers'));
const ProgressBar = loadable(() => import('../views/base/progress-bar/ProgressBar'));
const Switches = loadable(() => import('../views/base/switches/Switches'));

const Tabs = loadable(() => import('../views/base/tabs/Tabs'));
const Tooltips = loadable(() => import('../views/base/tooltips/Tooltips'));
const BrandButtons = loadable(() => import('../views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = loadable(() => import('../views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = loadable(() => import('../views/buttons/button-groups/ButtonGroups'));
const Buttons = loadable(() => import('../views/buttons/buttons/Buttons'));
const Charts = loadable(() => import('../views/charts/Charts'));
const Dashboard = loadable(() => import('../views/dashboard/Dashboard'));
const CoreUIIcons = loadable(() => import('../views/icons/coreui-icons/CoreUIIcons'));
const Flags = loadable(() => import('../views/icons/flags/Flags'));
const Brands = loadable(() => import('../views/icons/brands/Brands'));
const Alerts = loadable(() => import('../views/notifications/alerts/Alerts'));
const Badges = loadable(() => import('../views/notifications/badges/Badges'));
const Modals = loadable(() => import('../views/notifications/modals/Modals'));
const Colors = loadable(() => import('../views/theme/colors/Colors'));
const Typography = loadable(() => import('../views/theme/typography/Typography'));
const Widgets = loadable(() => import('../views/widgets/Widgets'));
const Users = loadable(() => import('../views/users/Users'));
const User = loadable(() => import('../views/users/User'));

const LecturersList = loadable(() => import('../views/lecturers/LecturersList'))
const SubjectsList = loadable(() => import('../views/subjects/SubjectsList'))
const GroupsList = loadable(() => import('../views/groups/GroupsList'))
const GroupForm = loadable(() => import('../views/groups/GroupForm'))

export const RootPath = {
  PAGE_LOGIN: `/login`,
  DASHBOARD: '/dashboard',
  LECTURERS: '/lecturers',
  GROUPS: '/groups',
  GROUP_FORM: '/groups/create',
  SUBJECTS: '/subjects',
  PAGE_404: '/404',
  PAGE_500: '/500'
}

export const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: RootPath.LECTURERS, exact: true,  name: 'List Lecturers', component: LecturersList },
  { path: RootPath.SUBJECTS, exact: true,  name: 'List Lecturers', component: SubjectsList },
  { path: `${RootPath.GROUPS}`, exact: true, name: 'Create Group', component: GroupForm },
  { path: `${RootPath.GROUPS}/:lecturer`, name: 'List Groups', component: GroupsList },
  { path: `${RootPath.GROUPS}/form`, name: 'Create Group', component: GroupForm },
  { path: `${RootPath.GROUPS}/form/:groupId`, name: 'Edit Groups', component: GroupForm },
];
