import React from 'react';
import {Redirect, Route} from "react-router-dom";
import PropTypes from "prop-types";
import {RootPath} from "./routes";
import {getToken} from "../utils/helpers/authUtils";

const PrivateRoute = ({ component: Component, path, ...rest}) => {
  const token = getToken();

  if (!token) {
    return <Redirect to={RootPath.PAGE_LOGIN}/>
  }
  return (
    <Route exact
           {...rest}
           key="route"
           path={path}
           render={(props) => <Component {...props} />}/>
  );
};

PrivateRoute.propTypes = {
  path: PropTypes.string,
}

PrivateRoute.defaultProps = {
  path: RootPath.PAGE_LOGIN,
}

export default PrivateRoute;
