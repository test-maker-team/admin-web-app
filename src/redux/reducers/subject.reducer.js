import * as actions from '../actions/subject.actions';

const initialState = {
  loading: false,
  error: false,
  code: 0,
  data: [],
  pageInfo: {
    currentPage: 0,
    itemsPerPage: 25,
    totalPages: 0
  }
}

export default function subjectReducer(state = initialState, { type, payload }) {
  switch (type) {
    case actions.FETCH_SUBJECT_REQUEST:
      return {
        ...state,
        loading: true
      };
    case actions.FETCH_SUBJECT_SUCCEEDED:
      return {
        ...state,
        loading: false,
        code: payload.code,
        data: payload.data,
        pageInfo: payload.pageInfo
      };
    case actions.FETCH_SUBJECT_FAILED:
      return {
        ...state,
        loading: false,
        error: payload.error
      };
    case actions.FETCH_SUBJECT_REFRESH:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
