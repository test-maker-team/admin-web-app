import * as authActions from './actions/auth.actions';
import configStore from './configStore';

const store = configStore();

const boot = async () => {
    await store.dispatch(authActions.checkAuthorization());
};

export default boot;
