import {combineReducers} from 'redux';

import authReducer from './reducers/auth.reducer';
import lecturerReducer from './reducers/lecturer.reducer';
import subjectReducer from './reducers/subject.reducer';
import sidebarReducer from "./reducers/sidebar.reducer";
import groupReducer from "./reducers/group.reducer";

const rootReducer = combineReducers({
    auth: authReducer,
    sidebar: sidebarReducer,
    lecturer: lecturerReducer,
    subject: subjectReducer,
    group: groupReducer,
})

export default rootReducer;
