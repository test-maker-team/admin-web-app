// Create Redux action types
export const FETCH_GROUP_REQUEST = 'FETCH_GROUP_REQUEST';
export const FETCH_GROUP_SUCCEEDED = 'FETCH_GROUP_SUCCEEDED';
export const FETCH_GROUP_FAILED = 'FETCH_GROUP_FAILED';
export const FETCH_GROUP_REFRESH = 'FETCH_GROUP_REFRESH';

// Create Redux action creators that return an action
export const fetchRequest = (data) => {
  return ({
    type: FETCH_GROUP_REQUEST,
    payload: data
  })
}
