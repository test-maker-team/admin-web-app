// Create Redux action types
export const FETCH_SUBJECT_REQUEST = 'FETCH_SUBJECT_REQUEST';
export const FETCH_SUBJECT_SUCCEEDED = 'FETCH_SUBJECT_SUCCEEDED';
export const FETCH_SUBJECT_FAILED = 'FETCH_SUBJECT_FAILED';
export const FETCH_SUBJECT_REFRESH = 'FETCH_SUBJECT_REFRESH';

// Create Redux action creators that return an action
export const fetchRequest = (data) => ({
  type: FETCH_SUBJECT_REQUEST,
  payload: data
})
