// Create Redux action types
export const FETCH_LECTURER_REQUEST = 'FETCH_LECTURER_REQUEST';
export const FETCH_LECTURER_SUCCEEDED = 'FETCH_LECTURER_SUCCEEDED';
export const FETCH_LECTURER_FAILED = 'FETCH_LECTURER_FAILED';
export const FETCH_LECTURER_REFRESH = 'FETCH_LECTURER_REFRESH';

// Create Redux action creators that return an action
export const fetchRequest = (data) => {
  return ({
    type: FETCH_LECTURER_REQUEST,
    payload: data
  })
}
